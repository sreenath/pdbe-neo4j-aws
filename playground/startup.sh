#!/bin/bash

CURRDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# update yum
yum update -y

# install dependencies
yum install $BASIC_DEPENDENCIES -y

# get Neo4J
wget $NEO4J_DOWNLOAD_URL
tar -xvf artifact.php?name=$NEO4J_SETUP_FILE
rm artifact.php?name=$NEO4J_SETUP_FILE -f
mv $NEO4J_VERSION /var/lib/

export NEO4J_HOME=/var/lib/$NEO4J_VERSION

# get apoc plugin
wget $APOC_URL
mv $APOC_VERSION $NEO4J_HOME/plugins/

### make few config changes for Neo4J
for line in `cat $CURRDIR/neo4j.conf`; do
    sed -i -e "1i$line\\" $NEO4J_HOME/conf/neo4j.conf
done

# provide ownership to ec2-user user
chown -R ec2-user:ec2-user $NEO4J_HOME

# echo "Use \"$NEO4J_HOME/bin/neo4j start\" to start Neo4J instance"
sudo -u ec2-user $NEO4J_HOME/bin/neo4j start
