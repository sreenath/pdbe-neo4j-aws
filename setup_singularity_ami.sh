# update yum
sudo yum update -y && \
    sudo yum groupinstall 'Development Tools' -y && \
    sudo yum install libarchive-devel -y

# install epel release
sudo amazon-linux-extras install epel -y

# setup singulaity
sudo yum update -y && \
    sudo yum install -y epel-release && \
    sudo yum update -y && \
    sudo yum install -y singularity-runtime singularity

