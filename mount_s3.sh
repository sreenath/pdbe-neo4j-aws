# update system
sudo yum update -y

# Install fuse and s3cmd and all its dependencies
sudo yum install -y automake fuse fuse-devel gcc-c++ git libcurl-devel libxml2-devel make openssl-devel

# Download s3fs source code from git
git clone https://github.com/s3fs-fuse/s3fs-fuse.git

# Compile and install the code
cd s3fs-fuse
./autogen.sh 
./configure --prefix=/usr --with-openssl
make 
sudo make install




s3fs -o iam_role="PDBeBucketWriter" -o url="https://s3-us-east-2.amazonaws.com" -o endpoint=us-east-2 -o dbglevel=info -o curldbg -o allow_other -o use_cache=/tmp ebi-pdbe /var/s3fs-demos