mountpoint=/media/disk1

# get compressed graph db from S3
echo "Getting compressed graph db from S3 - START" 
date
aws s3 --region=us-east-2 cp s3://ebi-pdbe/neo4j/graph.db.bzip2 $mountpoint/
date
echo "Getting compressed graph db from S3 - END" 

mv $mountpoint/graph.db.bzip2 $mountpoint/neo4j_data/databases/graph.db/

# uncompress graph data
echo "Uncompress graph db - START"
cd $mountpoint/neo4j_data/databases/graph.db
date
pbzip2 -dc graph.db.bzip2 | tar x
date
rm graph.db.bzip2
echo "Uncompress graph db - END"

# echo "Starting Neo4J"
$NEO4J_HOME/bin/neo4j start
