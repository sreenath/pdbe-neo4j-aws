#!/bin/bash

# update yum
yum update -y

# get the SSD device name
device=$(lsblk | grep nvme0n1 | cut -d' ' -f1)

# create an ext4 partition on the disk
mkfs.ext4 -E nodiscard -m0 /dev/$device

# create a mount point
mountpoint=/media/disk1
sudo mkdir -p $mountpoint

# mount the device
mount -o discard /dev/$device $mountpoint

# create directories for neo4j data
mkdir -p $mountpoint/neo4j_data/databases/graph.db
mkdir -p $mountpoint/neo4j_data/dbms

# provide ownership to ec2-user user
chown -R ec2-user:ec2-user $mountpoint
