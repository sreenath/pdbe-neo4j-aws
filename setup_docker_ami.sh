
# Update the installed packages
sudo yum update -y

# Install Docker Community Edition package
sudo amazon-linux-extras install docker -y

# Start Docker service
sudo service docker start

# Add ec2-user to docker group so that ec2-user can run Docker commands without sudo
sudo usermod -a -G docker ec2-user

