for machine in $(aws ec2 describe-instances --region us-east-2 --instance-ids --query Reservations[].Instances[].PublicIpAddress --filters "Name=tag:Name,Values=kb-highspec" --output=text); do
    echo "Invoking graph DB setup in $machine - STARTED"
    ssh -i "$HOME/pdbe-kp.pem" ec2-user@$machine "nohup ./get_db.sh > nohup.out 2>&1 &"
    echo "Invoking graph DB setup in $machine - ENDED"
done

