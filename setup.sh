
env=$1

[[ $env != "highspec" && $env != "playground" && $env != "release" ]] && echo "Usage: bash setup.sh <env (highspec, playground or release)>" && exit 1

echo "Setting up $env machine"

source common-vars.sh

./$env/startup.sh
