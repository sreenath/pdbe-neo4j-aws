
export PBZIP_BASE_URL="http://ftp.tu-chemnitz.de/pub/linux/dag/redhat/el7/en/x86_64/rpmforge/RPMS"
export PBZIP_PACKAGE="pbzip2-1.0.5-1.el7.rf.x86_64.rpm"
export BASIC_DEPENDENCIES="java-1.8.0-openjdk"
export NEO4J_VERSION="neo4j-community-3.5.13"
export NEO4J_SETUP_FILE="$NEO4J_VERSION-unix.tar.gz"
export NEO4J_DOWNLOAD_URL="https://neo4j.com/artifact.php?name=$NEO4J_SETUP_FILE"
export APOC_VERSION="apoc-3.5.0.6-all.jar"
export APOC_URL="https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/3.5.0.6/$APOC_VERSION"


