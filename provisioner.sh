#!/bin/bash

# update yum
sudo yum update -y

# install dependencies
sudo yum install pigz -y
sudo yum install java-1.8.0-openjdk -y

#  get Neo4J
wget https://neo4j.com/artifact.php?name=neo4j-community-3.5.13-unix.tar.gz
tar -xvf artifact.php?name=neo4j-community-3.5.13-unix.tar.gz
rm artifact.php?name=neo4j-community-3.5.13-unix.tar.gz
sudo mv neo4j-community-3.5.13 /var/lib/

export NEO4J_HOME=/var/lib/neo4j-community-3.5.13

# get apoc plugin
wget https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/3.5.0.6/apoc-3.5.0.6-all.jar
sudo mv apoc-3.5.0.6-all.jar $NEO4J_HOME/plugins/

### make few config changes for Neo4J

# disable authentication
sed -i -e '1idbms.security.auth_enabled=false\' $NEO4J_HOME/conf/neo4j.conf

# enable listen to address other than localhost
sed -i -e '1idbms.connectors.default_listen_address=0.0.0.0\' $NEO4J_HOME/conf/neo4j.conf

# provide only read only access to DB
sed -i -e '1idbms.read_only=true\' $NEO4J_HOME/conf/neo4j.conf

# enable apoc procedures/plugins
sed -i -e '1idbms.security.procedures.unrestricted=apoc.*\' $NEO4J_HOME/conf/neo4j.conf

# get the SSD device name
device=$(lsblk | grep nvme | cut -d' ' -f1)

# # create an ext4 partition on the disk
sudo mkfs.ext4 -E nodiscard -m0 /dev/$device

# # create a mount point
mountpoint=/media/nvme01
sudo mkdir -p $mountpoint

# # mount the device
sudo mount -o discard /dev/$device $mountpoint

# # provide ownership to ubuntu user
sudo chown ec2-user:ec2-user $mountpoint

# get compressed graph db from S3
echo "Getting compressed graph db from S3 - START" 
date
aws s3 cp s3://ebi-pdbe/test.graph.db.tgz $mountpoint/
echo " compressed graph db from S3 - END" 
date

# create directories for neo4j data
mkdir -p $mountpoint/neo4j_data/databases/graph.db
mkdir -p $mountpoint/neo4j_data/dbms
graphdb_path=$mountpoint/neo4j_data/databases/graph.db

# uncompress graph data
echo "Uncompress graph db - START"
date
tar -x --use-compress-program=pigz -f $mountpoint/test.graph.db.tgz -C $graphdb_path
rm $mountpoint/test.graph.db.tgz
echo "Uncompress graph db - END"
date

# Change Neo4J config to pick data from new location
sed -i -e '1idbms.directories.data='${mountpoint}'/neo4j_data\' $NEO4J_HOME/conf/neo4j.conf

echo "Starting Neo4J"
$NEO4J_HOME/bin/neo4j start

