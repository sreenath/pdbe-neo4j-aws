#!/bin/bash

# update yum
yum update -y

# install dependencies
yum install wget
wget http://ftp.tu-chemnitz.de/pub/linux/dag/redhat/el7/en/x86_64/rpmforge/RPMS/pbzip2-1.0.5-1.el7.rf.x86_64.rpm
yum install java-1.8.0-openjdk aws-cli pbzip2-1.0.5-1.el7.rf.x86_64.rpm -y
rm pbzip2-1.0.5-1.el7.rf.x86_64.rpm

#  get Neo4J
wget https://neo4j.com/artifact.php?name=neo4j-community-3.5.13-unix.tar.gz
tar -xvf artifact.php?name=neo4j-community-3.5.13-unix.tar.gz
rm artifact.php?name=neo4j-community-3.5.13-unix.tar.gz -f
mv neo4j-community-3.5.13 /var/lib/

export NEO4J_HOME=/var/lib/neo4j-community-3.5.13

# get apoc plugin
wget https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/3.5.0.6/apoc-3.5.0.6-all.jar
mv apoc-3.5.0.6-all.jar $NEO4J_HOME/plugins/

### make few config changes for Neo4J

# disable authentication
sed -i -e '1idbms.security.auth_enabled=false\' $NEO4J_HOME/conf/neo4j.conf

# enable listen to address other than localhost
sed -i -e '1idbms.connectors.default_listen_address=0.0.0.0\' $NEO4J_HOME/conf/neo4j.conf

# provide only read only access to DB
sed -i -e '1idbms.read_only=true\' $NEO4J_HOME/conf/neo4j.conf

# enable apoc procedures/plugins
sed -i -e '1idbms.security.procedures.unrestricted=apoc.*\' $NEO4J_HOME/conf/neo4j.conf

# set heap and page cache
sed -i -e '1idbms.memory.heap.initial_size=30g\' $NEO4J_HOME/conf/neo4j.conf
sed -i -e '1idbms.memory.heap.max_size=30g\' $NEO4J_HOME/conf/neo4j.conf
sed -i -e '1idbms.memory.pagecache.size=30g\' $NEO4J_HOME/conf/neo4j.conf

# get the SSD device name
device=$(lsblk | grep nvme1n1 | cut -d' ' -f1)

# create an ext4 partition on the disk
mkfs.ext4 -E nodiscard -m0 /dev/$device

# create a mount point
mountpoint=/media/disk1
mkdir -p $mountpoint

# mount the device
mount -o discard /dev/$device $mountpoint

# provide ownership to ubuntu user
chown ec2-user:ec2-user $mountpoint

# get compressed graph db from S3
echo "Getting compressed graph db from S3 - START" 
date
aws s3 cp s3://ebi-pdbe/neo4j/graph.db.tgz $mountpoint/
date
echo "Getting compressed graph db from S3 - END" 

# create directories for neo4j data
graphdb_path=$mountpoint/neo4j_data/databases/graph.db
mkdir -p $graphdb_path
mkdir -p $mountpoint/neo4j_data/dbms
mv $mountpoint/graph.db.tgz $graphdb_path/

# uncompress graph data
echo "Uncompress graph db - START"
cd $graphdb_path
date
pbzip2 -dc graph.db.tgz | tar x
# tar -x --use-compress-program=pigz -f $mountpoint/graph.db.tgz -C $graphdb_path
date
rm graph.db.tgz
echo "Uncompress graph db - END"


# Change Neo4J config to pick data from new location
sed -i -e '1idbms.directories.data='${mountpoint}'/neo4j_data\' $NEO4J_HOME/conf/neo4j.conf

echo "Starting Neo4J"
$NEO4J_HOME/bin/neo4j start

