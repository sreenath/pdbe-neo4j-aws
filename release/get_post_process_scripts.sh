
echo "Getting post process scripts"

aws s3 cp --recursive s3://ebi-pdbe/neo4j/cypher_queries/ cypher_queries

echo "Executing post process scripts"

cat cypher_queries/constraints.cql | $NEO4J_HOME/bin/cypher-shell -u neo4j -p pdbe_neo
cat cypher_queries/set_num_mapped_pdb_entries.cql | $NEO4J_HOME/bin/cypher-shell -u neo4j -p pdbe_neo
cat cypher_queries/rank_scoring.cql | $NEO4J_HOME/bin/cypher-shell -u neo4j -p pdbe_neo
cat cypher_queries/funpdbe_partners.cql | $NEO4J_HOME/bin/cypher-shell -u neo4j -p pdbe_neo

echo "Executed post process scripts"
